package com.adonis.gabriel.demo_app.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.adonis.gabriel.demo_app.Data.Model.ContentItem;
import com.adonis.gabriel.demo_app.Fragments.Adapters.ImageGalleryPagerAdapter;
import com.adonis.gabriel.demo_app.R;
import com.adonis.gabriel.demo_app.TrailerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsFragment extends Fragment {

    public static final String DEBUG_TAG = DetailsFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = "details";
    public static final String ARGUMENTS_KEY_CONTENT = "content";

    @BindView(R.id.tv_content_title)
    TextView mTitle;

    @BindView(R.id.vp_image)
    ViewPager mImageGlider;

    @Nullable
    @BindView(R.id.tv_description)
    TextView mDescription;

    @BindView(R.id.tv_popularity)
    TextView mLikes;

    @BindView(R.id.iv_share)
    ImageView mShare;

    @BindView(R.id.iv_map_pin)
    ImageView mLocate;

    private FragmentManager fragmentManager;


    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, inflatedView);

        fragmentManager = getActivity().getSupportFragmentManager();

        Bundle bundledContent = getArguments();
        ContentItem content = (ContentItem)bundledContent.getParcelable(ARGUMENTS_KEY_CONTENT);

        ImageGalleryPagerAdapter imageGalleryPagerAdapter = new ImageGalleryPagerAdapter(getChildFragmentManager(), content.posterPath);
        mImageGlider.setAdapter(imageGalleryPagerAdapter);

        populateScreen(content);
        hideKeyboard(inflatedView);

        return inflatedView;
    }

    private void hideKeyboard(View view) {
        InputMethodManager keyboard = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void populateScreen(ContentItem content) {
        if (content == null) {
            return;
        }
        mTitle.setText(content.title);
        mTitle.setSelected(true);
        mDescription.setText(content.overview);
        mLikes.setText(content.voteCount.toString());

//        Picasso.get().load(ContentFetcher.POSTER_BASE_URL + content.posterPath).into(mImage);
    }

    @OnClick(R.id.iv_share)
    void playTrailer() {
        Intent intent = new Intent(getContext(), TrailerActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_map_pin)
    void openWebsite() {
        Uri targetUri = constructWebsiteLink();
        Intent intent = new Intent(Intent.ACTION_VIEW, targetUri);
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(intent);
        }
        else {
            Log.d(DEBUG_TAG, "Couldn't start " + targetUri.toString());
        }
    }

    private Uri constructWebsiteLink() {
        String title = mTitle.getText().toString();
        String titleWikipedia = title.replace(" ", "_");
        String wikiBase = "https://en.wikipedia.org/wiki/";

        return Uri.parse(wikiBase+titleWikipedia);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "PAUSED");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(DEBUG_TAG, "STOPPED");
    }
}
