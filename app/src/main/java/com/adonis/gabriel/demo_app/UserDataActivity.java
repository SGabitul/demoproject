package com.adonis.gabriel.demo_app;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.adonis.gabriel.demo_app.Data.Model.UserEntity;
import com.adonis.gabriel.demo_app.Data.UserDatabase;
import com.adonis.gabriel.demo_app.Login.AppLogin;
import com.adonis.gabriel.demo_app.Login.Validation;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserDataActivity extends AppCompatActivity {

    public static final String DEBUG_TAG = UserDataActivity.class.getSimpleName();

    private interface FinishedLoginCallback {
        void finishedLogin( AccessToken accessToken);
    }

    @BindView(R.id.tv_last_name)
    EditText mLastName;

    @BindView(R.id.tv_first_name)
    EditText mFirstName;

    @BindView(R.id.tv_email)
    EditText mEmail;

    private CallbackManager mCallbackManager;

    private UserDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);

        ButterKnife.bind(this);

        mDatabase = UserDatabase.getInstance(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.bt_import_fb)
    void fetchFacebookData() {
        initFacebookLogin(Arrays.asList("email", "user_birthday", "user_gender"), new FinishedLoginCallback() {
            @Override
            public void finishedLogin(AccessToken accessToken) {
                importData(accessToken,  "first_name, last_name, email, picture, birthday, gender");
            }
        });
    }

    private void initFacebookLogin(Collection<String> permissions, final FinishedLoginCallback finishedLoginCallback) {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager loginManager = LoginManager.getInstance();
        loginManager.logInWithReadPermissions(this, permissions);

        loginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if(finishedLoginCallback!=null){
                    finishedLoginCallback.finishedLogin(loginResult.getAccessToken());
                }
            }

            @Override
            public void onCancel() {
                //Don't do nuffin?
            }

            @Override
            public void onError(FacebookException error) {
                //TODO display a pop-up telling the user that shit has gone horribly wrong
            }
        });
    }

    /**
     * Imports facebook data given in the fields and populates the views and adds the data to the database
     * @param userAccessToken the access toke
     * @param fields the GraphQL fields of the data
     */
    private void importData(AccessToken userAccessToken, String fields) {
        GraphRequest request = GraphRequest.newMeRequest(
                userAccessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        /**
                         * IMP you're not using the fields parameter
                         * create a FacebookDataConverter to do all this
                         */
                        Log.d(DEBUG_TAG, object.toString());
                        String firstName;
                        String lastName;
                        try {
                            firstName = object.getString("first_name");
                            lastName = object.getString("last_name");
                        } catch (JSONException e) {
                            firstName = null;
                            lastName = null;
                            Log.d(DEBUG_TAG, "This shouldn't happen I think");
                        }
                        String email;
                        try {
                            email = object.getString("email");
                        } catch (JSONException e) {
                            Log.d(DEBUG_TAG, "Failed to get mail");
                            email = null;
                        }
                        String birthday;
                        try {
                            birthday = object.getString("birthday");
                        } catch (JSONException e) {
                            Log.d(DEBUG_TAG, "Failed to get birthday");
                            birthday = null;
                        }

                        String gender;
                        try {
                            gender = object.getString("gender");
                        } catch (JSONException e) {
                            Log.d(DEBUG_TAG, "Failed to get gender");
                            gender = null;
                        }
                        JSONObject pictureData;
                        try {
                            pictureData = object.getJSONObject("picture").getJSONObject("data");
                        } catch (JSONException e) {
                            Log.d(DEBUG_TAG, "Failed to get picture");
                            pictureData = null;
                        }
                        String pictureUrl;
                        try {
                            pictureUrl = pictureData.getString("url");
                        } catch (JSONException e) {
                            Log.d(DEBUG_TAG, "Failed to get pictureUrl");
                            pictureUrl = null;
                        }

                        //IMP instead of passing each attribute construct the object here and pass that
                        registerAndMoveOn(firstName, lastName, birthday, gender, email, pictureUrl);
                    }
                }
        );
        Bundle parameteres = new Bundle();
        parameteres.putString("fields", fields);
        request.setParameters(parameteres);
        request.executeAsync();
    }

    /**
     * Populates the views that the user would normally edit and then triggers the register callback
     * @param first_name user's first name
     * @param last_name user's last name
     * @param email user's email
     */
    private void populateViews(String first_name, String last_name, String email) {
        mFirstName.setText(first_name);
        mLastName.setText(last_name);
        mEmail.setText(email);
    }


    @OnClick(R.id.bt_register)
    void registerAndMoveOn() {
        boolean userRegistered = registerUser();
        if (userRegistered) {
            MainActivity.startMainActivity(this);
        }
    }

    void registerAndMoveOn(String firstName, String lastName, String birthday, String gender, String email, String pictureUrl) {
        boolean userRegistered = registerUser(firstName, lastName, birthday, gender, email, pictureUrl);
        if (userRegistered) {
            MainActivity.startMainActivity(this);
        }
    }

    /**
     * Used when the user imports his data from Facebook
     */
    private boolean registerUser(String firstName, String lastName, String birthday, String gender, String email, String pictureUrl) {
        Intent startIntent = getIntent();

        if (! startIntent.hasExtra(getString(R.string.tag_phone))) {
            //QUIRK if this happens CRASH AND COMPLAIN
            Log.d(DEBUG_TAG, "This intent should have a phone number extra");
            return false;
        }
        String phoneNumber = startIntent.getStringExtra(getString(R.string.tag_phone));

        if (! startIntent.hasExtra(getString(R.string.tag_password))) {
            //QUIRK if this happens CRASH AND COMPLAIN
            Log.d(DEBUG_TAG, "This intent should have a password extra");
            return false;
        }
        String password = startIntent.getStringExtra(getString(R.string.tag_password));

        Date birthDate;
        if (birthday == null) {
            birthDate = null;
        }
        else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            try {
                birthDate = dateFormat.parse(birthday);
            } catch (ParseException e) {
                //TODO do something if the birthday can't be parsed
                Log.d(DEBUG_TAG, "Couldn't parse the provided birthday");
                birthDate = null;
            }
        }

        UserEntity user = new UserEntity(
                phoneNumber,
                password,
                lastName,
                firstName,
                birthDate,
                gender,
                email,
                pictureUrl
        );

        new AsyncTask<UserEntity, Void, UserEntity>() {
            @Override
            protected UserEntity doInBackground(UserEntity... users) {
                UserEntity user = users[0];
                mDatabase.userDao().insertUser(user);
                UserEntity registeredUser = mDatabase.userDao().getUser(user.getPhoneNumber());
                return registeredUser;
            }

            @Override
            protected void onPostExecute(UserEntity user) {
                AppLogin.setLoginState(UserDataActivity.this, AppLogin.LOGIN_STATE_USER, user.getId());
            }
        }.execute(user);

        Log.d(DEBUG_TAG, "User registered to database");
        return true;
    }

    /**
     * Used when the user enters his profile data manually
     */
    private boolean registerUser() {
        String lastName = mLastName.getText().toString();
        if (! Validation.nameValid(lastName)) {
            //TODO to be replaced
            Toast.makeText(this, getString(R.string.last_name_invalid), Toast.LENGTH_SHORT).show();
            return false;
        }

        String firstName = mFirstName.getText().toString();
        if (! Validation.nameValid(firstName)) {
            //TODO to be replaced
            Toast.makeText(this, getString(R.string.first_name_invalid), Toast.LENGTH_SHORT).show();
            return false;
        }

        String email = mEmail.getText().toString();
        if (! Validation.emailValid(email)) {
            //TODO to be replaced
            Toast.makeText(this, getString(R.string.email_invalid), Toast.LENGTH_SHORT).show();
            return false;
        }

        return registerUser(firstName, lastName, null, null, email, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDatabase.close();
    }

}
