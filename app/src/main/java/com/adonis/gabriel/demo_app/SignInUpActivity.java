package com.adonis.gabriel.demo_app;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adonis.gabriel.demo_app.Data.Model.UserEntity;
import com.adonis.gabriel.demo_app.Data.UserDatabase;
import com.adonis.gabriel.demo_app.Login.AppLogin;
import com.adonis.gabriel.demo_app.Login.Validation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


//TODO after the user logs in, he shouldn't be able to return to this activity unless he signs out
/** QUIRK make the phone number / password fields get focus even if the user clicks outside of them
 *  on the surrounding ViewGroup
 */
public class SignInUpActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = SignInUpActivity.class.getSimpleName();

    @BindView(R.id.tv_phone_number)
    EditText mPhoneNumber;

    @BindView(R.id.ll_password)
    LinearLayout mPassword;

    @BindView(R.id.tv_skip_login)
    TextView mSkipLogin;

    @BindView(R.id.tv_nothanks)
    TextView mNoThanks;

    private UserDatabase mDatabase;

    private Validation.UserRegisteredCallback mUserRegisteredCallback = new Validation.UserRegisteredCallback() {

        @Override
        public void onUserRegistered(Activity currentActivity, UserEntity user) {
            String phoneNumber = user.getPhoneNumber();
            String userPassword = user.getPassword();
            int userId = user.getId();
            if (mPassword.getVisibility() == View.VISIBLE) {
                String enteredPassword = ((EditText)mPassword.findViewById(R.id.tv_password)).getText().toString();
                if (userPassword.equals(enteredPassword)) {
                    AppLogin.setLoginState(currentActivity, AppLogin.LOGIN_STATE_USER, userId);
                    MainActivity.startMainActivity(currentActivity);
                } else {
                    showAlert(R.string.password_wrong);
                }
            }
            else {
                showPasswordField();
                hideSkip();
            }
        }


        @Override
        public void onUserNotRegistered(final Activity currentActivity, final String phoneNumber) {
            Intent intent = new Intent(currentActivity, ChoosePasswordActivity.class);
            // pass the phone number to the ChoosePasswordActivity
            intent.putExtra(getString(R.string.tag_phone), phoneNumber);
            startActivity(intent);
        }
    };


    /* Toast message for invalid password alert
     */
    private Toast mAlertToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_up);

        ButterKnife.bind(this);

        mDatabase = UserDatabase.getInstance(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        hidePasswordField();
        skipIfLoggedIn();
    }

    private void skipIfLoggedIn() {
        String loginState = AppLogin.getLoginState(this);
        if (loginState.equals(AppLogin.LOGIN_STATE_USER)) {
            MainActivity.startMainActivity(this);
        }
    }

    /** IMP make the skip button more sensitive
     * rn shit is hard to click
     */
    @OnClick(R.id.tv_skip_login)
    void skipLogin() {
        AppLogin.setLoginState(this, AppLogin.LOGIN_STATE_GUEST, AppLogin.LOGIN_USER_MISSING);
        MainActivity.startMainActivity(this);
    }

    @OnClick(R.id.bt_signup)
    void signup() {
        String phoneNumber = mPhoneNumber.getText().toString();

        // phone number valid
        if (Validation.phoneNumberValid(phoneNumber)) {
            Validation.isRegistered(phoneNumber, mDatabase, mUserRegisteredCallback, this);
        }
        // phone number is not valid
        else {
            showAlert(R.string.number_invalid);
        }
    }

    private void showPasswordField() {
        mPassword.setVisibility(View.VISIBLE);
    }

    private void hidePasswordField() {
        mPassword.setVisibility(View.GONE);
    }

    private void hideSkip() {
        mNoThanks.setVisibility(View.GONE);
        mSkipLogin.setVisibility(View.GONE);
    }

    private void showAlert(int resId) {
        /* Hide the previous toast message if it's still on the screen
         */
        if (mAlertToast != null) {
            mAlertToast.cancel();
        }
        mAlertToast = Toast.makeText(this, resId, Toast.LENGTH_SHORT);
        mAlertToast.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mDatabase.close();
    }


}
