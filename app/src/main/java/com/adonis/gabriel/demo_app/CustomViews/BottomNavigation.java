package com.adonis.gabriel.demo_app.CustomViews;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.Menu;

public interface BottomNavigation {
    /**
     * For now you're supposed to cast the listener into the type of listener your BottomNavigation
     * implementation supports
     */
    void setOnNavigationItemSelected(Object listener);

    void setTintList(ColorStateList list);

    void disableTint(Context context, int itemId);

    void removeItem(int itemId);

    void disableItem(int itemId);

    void setSelectedItemId(int id);

    int getSelectedItemId();

    void setItemIcon(@NonNull Drawable icon, @NonNull int menuItemId);

    Drawable getItemIcon(@NonNull int menuItemId);

    Menu getMenu();

    void inflateMenu(Context context, @NonNull int resId);

    void setBackground(Drawable background);

    void setBackgroundColor(int color);

    void setBackgroundResource(int resId);

    void hideTitles();

    void showTitles();
}