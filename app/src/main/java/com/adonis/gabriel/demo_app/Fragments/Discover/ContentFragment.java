package com.adonis.gabriel.demo_app.Fragments.Discover;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.EventLog;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.adonis.gabriel.demo_app.CustomViews.EventDispatcher;
import com.adonis.gabriel.demo_app.Data.ContentFetcher;
import com.adonis.gabriel.demo_app.Data.Model.ContentItem;
import com.adonis.gabriel.demo_app.Data.Model.ContentResponse;
import com.adonis.gabriel.demo_app.Fragments.Adapters.ContentAdapter;
import com.adonis.gabriel.demo_app.Fragments.DiscoverFragment;
import com.adonis.gabriel.demo_app.MainActivity;
import com.adonis.gabriel.demo_app.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ContentFragment extends Fragment {

    public static final String DEBUG_TAG = ContentFragment.class.getSimpleName();

    private Unbinder unbinder;

    private EventDispatcher eventDispatcher;

    @BindView(R.id.rv_content)
    RecyclerView mContent;

    @BindView(R.id.pb_waiter)
    ProgressBar mProgressBar;

    @BindView(R.id.ll_search)
    ViewGroup mSearch;

    private ContentAdapter mContentAdapter;

    private Callback<ContentResponse> mOnContentFetchFinishCallback = new Callback<ContentResponse>() {

        @Override
        public void onResponse(Call<ContentResponse> call, Response<ContentResponse> response) {
            if (response.isSuccessful()) {
                ContentResponse responseBody = response.body();
                List<ContentItem> data = responseBody.results;
                mContentAdapter.setData(data);
                mContentAdapter.notifyDataSetChanged();
                hideProgressBar();
            } else {
                //TODO display toast with error message
                Log.d(DEBUG_TAG, response.errorBody().toString());
            }
        }

        @Override
        public void onFailure(Call<ContentResponse> call, Throwable t) {

        }
    };
    private Call<ContentResponse> mContentFetchCall;


    public ContentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "Attached to activty");
        eventDispatcher = (EventDispatcher) activity;
        final MutableLiveData<Integer> dispatcher = eventDispatcher.getEventDispatcher();
        dispatcher.observe(this, new Observer<Integer>() {

            @Override
            public void onChanged(@Nullable Integer integer) {
                Log.d(DEBUG_TAG, "Event received");
                if (integer == EventDispatcher.EVENT_ID_SEARCH_CLICKED) {
                    toggleSearch();
                    dispatcher.setValue(EventDispatcher.EVENT_ID_NONE);
                }
            }
        }) ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedView = inflater.inflate(R.layout.fragment_content_base, container, false);
        unbinder = ButterKnife.bind(this, inflatedView);

        initializeRecyclerView();

        mContentFetchCall = ContentFetcher.enqueueFetch(getContentPageNumber(), mOnContentFetchFinishCallback);


        return inflatedView;
    }

    protected abstract int getContentPageNumber();

    private void initializeRecyclerView() {
        //IMP this is so bad
        FragmentManager activityFm = getActivity().getSupportFragmentManager();
        String currentActiveFragment = activityFm.getBackStackEntryAt(activityFm.getBackStackEntryCount() - 1).getName();

        GridLayoutManager layoutManager = (GridLayoutManager)mContent.getLayoutManager();
        if (currentActiveFragment.equals(DiscoverFragment.FRAGMENT_TAG)) {
            layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (position % 3 == 0) {
                        return 2;
                    }
                    return 1;
                }
            });
        }

        mContent.setLayoutManager(layoutManager);
        mContentAdapter = new ContentAdapter(activityFm, currentActiveFragment);
        mContent.setAdapter(mContentAdapter);
    }

    private void hideSearch() {
        mSearch.setVisibility(View.GONE);
    }

    private void showSearch() {
        mSearch.setVisibility(View.VISIBLE);
    }

    private void toggleSearch() {
        boolean searchIsVisible = mSearch.getVisibility() == View.VISIBLE;
        if (searchIsVisible) {
            hideSearch();
        } else {
            showSearch();
        }
    }

    private void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);
    }

    private void displayProgressBar() {
        mContent.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mContentFetchCall != null) {
            mContentFetchCall.cancel();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
