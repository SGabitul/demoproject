package com.adonis.gabriel.demo_app.CustomViews;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Region;

import com.squareup.picasso.Transformation;

public class CircleTransform implements Transformation {

    public static final String KEY_CIRCLE_TRANSFORM = "circle_transform";

    @Override
    public Bitmap transform(Bitmap source) {
        int height = source.getHeight();
        int width = source.getWidth();
        Bitmap plane = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(plane);

        Path clipPath = new Path();
        float centerX = width / 2;
        float centerY = height / 2;
        // we make the radius a bit smaller so that it doesn't get clipped
        float radius = height / 2 - 2;
        clipPath.addCircle(centerX, centerY, radius, Path.Direction.CCW);

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        canvas.drawPath(clipPath, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, 0, 0, paint);

        source.recycle();
        return plane;
    }

    @Override
    public String key() {
        return KEY_CIRCLE_TRANSFORM;
    }
}
