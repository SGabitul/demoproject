package com.adonis.gabriel.demo_app.CustomViews;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;

import com.adonis.gabriel.demo_app.R;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import java.util.ArrayList;
import java.util.List;

public class AHBottomNavigationHelper implements BottomNavigation {
    @Override
    public void setBackground(Drawable background) {
        bottomNavigation.setBackground(background);
    }

    @Override
    public void setBackgroundColor(int color) {
        bottomNavigation.setDefaultBackgroundColor(color);
    }

    @Override
    public void setBackgroundResource(int resId) {
        bottomNavigation.setDefaultBackgroundResource(resId);
    }

    @Override
    public void hideTitles() {
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
    }

    @Override
    public void showTitles() {
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
    }

    @Override
    public void setOnNavigationItemSelected(Object listener) {
        AHBottomNavigation.OnTabSelectedListener actualListener = null;
        if (listener instanceof AHBottomNavigation.OnTabSelectedListener) {
            actualListener = (AHBottomNavigation.OnTabSelectedListener)listener;
        }
        bottomNavigation.setOnTabSelectedListener(actualListener);
    }

    @Override
    public void setTintList(ColorStateList list) {
        if (list == null) {
            return;
        }

        int defaultColor = list.getDefaultColor();
        int[] stateChecked = {android.R.attr.state_checked};
        int[] stateUnchecked = {-android.R.attr.state_checked};
        int colorActive = list.getColorForState(stateChecked, defaultColor);
        int colorInactive = list.getColorForState(stateUnchecked, defaultColor);
        bottomNavigation.setAccentColor(colorActive);
        bottomNavigation.setInactiveColor(colorInactive);
        bottomNavigation.setItemDisableColor(colorInactive);
    }

    @Override
    public void disableTint(Context context, int itemId) {
        int itemPos = translateItemIdToPosition(itemId);
        if (itemPos == -1) {
            return;
        }
        AHBottomNavigationItem item = bottomNavigation.getItem(itemPos);
        item.setDodgeTint(true);
    }

    @Override
    public void removeItem(int itemId) {
        int itemPos = translateItemIdToPosition(itemId);
        if (itemPos == -1) {
            return;
        }
        if (backendMenu.findItem(itemId) == null) {
            return;
        }

        backendMenu.removeItem(itemId);
        bottomNavigation.removeItemAtIndex(itemPos);
    }

    @Override
    public void disableItem(int itemId) {
        int itemPos = translateItemIdToPosition(itemId);
        if (itemPos == -1) {
            return;
        }
        if (backendMenu.findItem(itemId) == null) {
            return;
        }

        bottomNavigation.disableItemAtPosition(itemPos);
    }

    @Override
    public void setSelectedItemId(int id) {
        final int itemPosition = translateItemIdToPosition(id);
        if (itemPosition == -1) {
            return;
        }

        bottomNavigation.setCurrentItem(itemPosition);

        clearAllViewsBackground();
        View view = bottomNavigation.getViewAtPosition(itemPosition);
        if (view != null) {
            view.setBackgroundResource(R.drawable.bottom_bar);
        }
    }

    private void clearAllViewsBackground() {
        View view;
        for (int i = 0; i < bottomNavigation.getItemsCount(); i++) {
            view = bottomNavigation.getViewAtPosition(i);
            if (view != null) {
                view.setBackground(null);
            }
        }
    }

    @Override
    public int getSelectedItemId() {
        int currentItem = bottomNavigation.getCurrentItem();
        return translateItemPositionToId(currentItem);
    }

    private int translateItemPositionToId(int position) {
        List<MenuItem> menuItems = extractMenuItems(backendMenu);
        return menuItems.get(position).getItemId();
    }

    /**
     * If the item is not found, nothing happens
     */
    @Override
    public void setItemIcon(@NonNull Drawable icon, @NonNull int menuItemId) {
        int itemPosition = translateItemIdToPosition(menuItemId);
        if (itemPosition == -1) {
            return;
        }
        bottomNavigation.getItem(itemPosition).setDrawable(icon);
        bottomNavigation.refresh();
    }

    /**
     * This works on the assumption that the order of the items in AHBottomNavigation is the same
     * as when I added them
     * Return -1 if the menu item is not found
     */
    private int translateItemIdToPosition(int menuItemId) {
        List<MenuItem> menuItems = extractMenuItems(backendMenu);
        for (int i = 0; i < menuItems.size(); i++) {
            if (menuItems.get(i).getItemId() == menuItemId) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public Drawable getItemIcon(@NonNull int menuItemId) {
        return backendMenu.getItem(menuItemId).getIcon();
    }

    @Override
    public Menu getMenu() {
        return backendMenu;
    }

    @Override
    public void inflateMenu(Context context, @NonNull int resId) {
        bottomNavigation.removeAllItems();

        List<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();
        List<MenuItem> menuItems = extractMenuItems(backendMenu);
        for (MenuItem it : menuItems) {
            String title = it.getTitle().toString();
            Drawable icon = it.getIcon();

            AHBottomNavigationItem item = new AHBottomNavigationItem(title, icon);
            bottomNavigationItems.add(item);
        }

        bottomNavigation.addItems(bottomNavigationItems);
    }

    private void createMenu(Context context, int resId) {
        // trick to get a Menu instance
        // all I want are the menu items anyways
        PopupMenu aux = new PopupMenu(context, null);
        Menu menu = aux.getMenu();

        MenuInflater inflater = new MenuInflater(context);
        inflater.inflate(resId, menu);

        backendMenu = menu;
    }

    private List<MenuItem> extractMenuItems(Menu menu) {
        if (menu == null) {
            return null;
        }

        ArrayList<MenuItem> menuItems = new ArrayList<>();
        for (int i = 0; i < menu.size(); i++) {
            menuItems.add(menu.getItem(i));
        }

        return menuItems;
    }

    private Menu backendMenu;

    private AHBottomNavigation bottomNavigation;

    public AHBottomNavigationHelper(@NonNull AHBottomNavigation view, Context context, int menuRes) {
        bottomNavigation = view;
        createMenu(context, menuRes);
        inflateMenu(context, menuRes);
    }

    public AHBottomNavigation getBottomNavigation() {
        return bottomNavigation;
    }

    public void setBottomNavigation(AHBottomNavigation bottomNavigation) {
        this.bottomNavigation = bottomNavigation;
    }
}
