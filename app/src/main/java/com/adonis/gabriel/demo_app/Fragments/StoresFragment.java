package com.adonis.gabriel.demo_app.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adonis.gabriel.demo_app.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoresFragment extends SupportMapFragment implements OnMapReadyCallback {

    public static final String FRAGMENT_TAG = "stores";

    public static final String LOCATION_TITLE_UNIVERSITY = "University";
    public static final String LOCATION_TITLE_HOME = "Home";
    public static final String LOCATION_TITLE_WORK = "Work";

    public static final LatLng LOCATION_UNIVERSITY = new LatLng(44.4360017, 26.1008772);
    public static final LatLng LOCATION_HOME = new LatLng(44.4836628, 26.0396953);
    public static final LatLng LOCATION_WORK = new LatLng(44.4479468, 26.1007252);

    //used for the camera zoom
    private static final float ZOOM_LEVEL_DEFAULT = 10;
    //default camera location (Bucharest)
    public static final LatLngBounds LOCATION_DEFAULT_BOUNDS =
            new LatLngBounds(new LatLng(44.352548,25.970629),
                             new LatLng(44.530071,26.224988));

    public StoresFragment() {
        // Required empty public constructor
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        BitmapDescriptor customIcon = BitmapDescriptorFactory.fromResource(R.drawable.heart);

        MarkerOptions markerUniversity = new MarkerOptions()
                .position(LOCATION_UNIVERSITY)
                .title(LOCATION_TITLE_UNIVERSITY)
                .icon(customIcon);

        MarkerOptions markerHome = new MarkerOptions()
                .position(LOCATION_HOME)
                .title(LOCATION_TITLE_HOME)
                .icon(customIcon);

        MarkerOptions markerWork = new MarkerOptions()
                .position(LOCATION_WORK)
                .title(LOCATION_TITLE_WORK)
                .icon(customIcon);

        CameraUpdate centerCity = CameraUpdateFactory.newLatLngZoom(LOCATION_DEFAULT_BOUNDS.getCenter(), ZOOM_LEVEL_DEFAULT);

        googleMap.moveCamera(centerCity);

        googleMap.addMarker(markerHome);
        googleMap.addMarker(markerUniversity);
        googleMap.addMarker(markerWork);
    }

}
