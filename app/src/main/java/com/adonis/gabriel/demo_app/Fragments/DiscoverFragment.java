package com.adonis.gabriel.demo_app.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adonis.gabriel.demo_app.Fragments.Discover.AllFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.CouponsFragment;
import com.adonis.gabriel.demo_app.Fragments.Adapters.ContentPagerAdapter;
import com.adonis.gabriel.demo_app.Fragments.Discover.NewFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.SalesFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.UrgentFragment;
import com.adonis.gabriel.demo_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverFragment extends Fragment {

    private static final int CONTENT_FRAGMENTS_COUNT = 5;
    public static final String FRAGMENT_TAG = "discover";
    private static final String DEBUG_TAG = DiscoverFragment.class.getSimpleName();

    @BindView(R.id.vp_content_glider)
    ViewPager mFragmentGlider;

    private static final Class[] mViewPagerFragments = {AllFragment.class,
            CouponsFragment.class, NewFragment.class, SalesFragment.class, UrgentFragment.class};

    public DiscoverFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  contentView = inflater.inflate(R.layout.fragment_discover, container, false);

        ButterKnife.bind(this, contentView);

        // THIS SHIT IS SO STUPID... What's the difference between this one and getActivity().getFragmentManager()?
        FragmentManager fragmentManager = getChildFragmentManager();
        mFragmentGlider.setAdapter(new ContentPagerAdapter(fragmentManager, mViewPagerFragments));

        return contentView;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "PAUSED");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(DEBUG_TAG, "STOPPED");
    }
}
