package com.adonis.gabriel.demo_app;

import android.os.Bundle;
import android.util.Log;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrailerActivity extends YouTubeBaseActivity {

    public static final String DEBUG_TAG = TrailerActivity.class.getSimpleName();

    @BindView(R.id.yt_player)
    YouTubePlayerView mYoutubePlayerView;

    private YouTubePlayer mYoutubePlayer;

    private YouTubePlayer.PlayerStateChangeListener mPlayerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onLoading() {

        }

        @Override
        public void onLoaded(String s) {
            mYoutubePlayer.play();
        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }
    };

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_trailer);
        ButterKnife.bind(this);

        mYoutubePlayerView.initialize("AIzaSyAVy7muhOMA0zUrY-Kh3LFhTGT2DsZwpNo", new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
                mYoutubePlayer = youTubePlayer;
                youTubePlayer.cueVideo("6ZfuNTqbHE8");
                youTubePlayer.setPlayerStateChangeListener(mPlayerStateChangeListener);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Log.d(DEBUG_TAG, "This shit ain't working");
            }
        });
    }
}
