package com.adonis.gabriel.demo_app.Fragments.Adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adonis.gabriel.demo_app.Data.ContentFetcher;
import com.adonis.gabriel.demo_app.Data.Model.ContentItem;
import com.adonis.gabriel.demo_app.Fragments.DetailsFragment;
import com.adonis.gabriel.demo_app.Fragments.ProfileFragment;
import com.adonis.gabriel.demo_app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//TODO move this inside its own class because it's used somewhere else as well
public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.ItemViewHolder> {

    private static final int CONTENT_BIG_TYPE = 1;
    private static final int CONTENT_SMALL_TYPE = 2;

    private List<ContentItem> data;

    private FragmentManager fragmentManager;

    private String mainFragmentTag;

    public ContentAdapter() {
        this.data = null;
        this.fragmentManager = null;
        this.mainFragmentTag = null;
    }

    public ContentAdapter(FragmentManager fragmentManager, String mainFragmentTag) {
        this.fragmentManager = fragmentManager;
        this.data = null;
        this.mainFragmentTag = mainFragmentTag;
    }

    public ContentAdapter(List<ContentItem> data) {
        this.data = data;
        this.fragmentManager = null;
        this.mainFragmentTag = null;
    }

    public void setData(List<ContentItem> data) {
        this.data = data;
    }

    public List<ContentItem> getData() {
        return this.data;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //IMP this is so sad
        if (mainFragmentTag.equals(ProfileFragment.FRAGMENT_TAG)) {
            viewType = CONTENT_SMALL_TYPE;
        }

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View inflatedView;
        if (viewType == CONTENT_BIG_TYPE) {
            inflatedView = inflater.inflate(R.layout.content_big, parent, false);
        }
        else {
            inflatedView = inflater.inflate(R.layout.content_small, parent, false);
        }

        return new ItemViewHolder(inflatedView);
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 3 == 0) {
            return CONTENT_BIG_TYPE;
        }

        return CONTENT_SMALL_TYPE;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (data == null) {
            return;
        }

        final ContentItem currentItem = data.get(position);

        holder.mTitle.setText(currentItem.title);
        holder.mTitle.setSelected(true);
        holder.mLikes.setText(currentItem.voteCount.toString());
        if (holder.hasDescription()) {
            holder.mDescription.setText(currentItem.overview);
        }

        String imageLink = currentItem.posterPath;
        Picasso.get().load(ContentFetcher.POSTER_BASE_URL + imageLink).into(holder.mImage);

        holder.mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailsFragment detailsFragment = new DetailsFragment();

                Bundle bundledData = new Bundle();
                bundledData.putParcelable(DetailsFragment.ARGUMENTS_KEY_CONTENT, currentItem);
                detailsFragment.setArguments(bundledData);

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragment_container ,detailsFragment);
                transaction.addToBackStack(DetailsFragment.FRAGMENT_TAG);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }

        return data.size();
    }



    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_content_title)
        TextView mTitle;

        @BindView(R.id.vp_image)
        ImageView mImage;

        @Nullable
        @BindView(R.id.tv_description)
        TextView mDescription;

        @BindView(R.id.tv_popularity)
        TextView mLikes;

        @BindView(R.id.iv_share)
        ImageView mShare;

        @BindView(R.id.iv_map_pin)
        ImageView mLocate;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public boolean hasDescription() {
            return !(mDescription == null);
        }
    }

}