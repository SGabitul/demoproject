package com.adonis.gabriel.demo_app.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.adonis.gabriel.demo_app.R;

/**
 * The login state is necessary for determining the profile screen info
 */
public class AppLogin {
    public static final String LOGIN_STATE_USER = "user";
    public static final String LOGIN_STATE_GUEST = "guest";
    public static final String LOGIN_STATE_UNSIGNED = "unsigned";
    public static final String LOGIN_USER_ID = "user_id";
    public static final Integer LOGIN_USER_MISSING = -1;

    /**
     * Sets the application login state along with the logged in user id
     * @param context the application context
     * @param mode the mode: LOGIN_STATE_USER, LOGIN_STATE_GUEST
     * @param userId the id of the logged in user or null if the mode is LOGIN_STATE_GUEST
     */
    public static void setLoginState(Context context, String mode, @Nullable Integer userId) {
        SharedPreferences.Editor spEdit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        spEdit.putString(context.getString(R.string.login_mode), mode);
        if (mode.equals(LOGIN_STATE_GUEST)) {
            userId = LOGIN_USER_MISSING;
        }
        spEdit.putInt(LOGIN_USER_ID, userId);
        spEdit.apply();
    }

    /**
     * Gets the application login state
     * @param context the application context
     * @return one of the values LOGIN_STATE_USER, LOGIN_STATE_GUEST, LOGIN_STATE_UNSIGNED
     */
    public static String getLoginState(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        // the default value is LOGIN_STATE_UNSIGNED
        return sp.getString(context.getString(R.string.login_mode), LOGIN_STATE_UNSIGNED);
    }

    /**
     * Get the logged in user's id
     * @param context the application context
     * @return the user id of the currently logged in user or LOGIN_USER_MISSING (-1)
     */
    public static Integer getLoginUserId(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getInt(LOGIN_USER_ID, LOGIN_USER_MISSING);
    }

}
