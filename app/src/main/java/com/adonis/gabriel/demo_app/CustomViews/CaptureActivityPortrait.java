package com.adonis.gabriel.demo_app.CustomViews;

import android.os.Bundle;
import android.widget.TextView;

import com.adonis.gabriel.demo_app.R;
import com.journeyapps.barcodescanner.CaptureActivity;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CaptureActivityPortrait extends CaptureActivity {

    @BindView(R.id.zxing_status_view)
    TextView mStatusText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        reinitializeUi();
    }

    private void reinitializeUi() {
        mStatusText.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
    }
}
