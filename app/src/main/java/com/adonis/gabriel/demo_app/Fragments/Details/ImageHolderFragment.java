package com.adonis.gabriel.demo_app.Fragments.Details;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adonis.gabriel.demo_app.Data.ContentFetcher;
import com.adonis.gabriel.demo_app.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageHolderFragment extends Fragment {

    public static final String IMAGE_KEY = "image";

    @BindView(R.id.vp_image)
    ImageView mImage;

    public ImageHolderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.fragment_image_holder, container, false);
        ButterKnife.bind(this, inflatedView);

        return inflatedView;
    }

    @Override
    public void onStart() {
        super.onStart();
        displayImage();
    }

    private void displayImage() {
        Bundle args = getArguments();
        String imageUrl = args.getString(IMAGE_KEY);

        Picasso.get()
                .load(ContentFetcher.POSTER_BASE_URL + imageUrl)
                .into(mImage);
    }
}
