package com.adonis.gabriel.demo_app.Fragments.Discover;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adonis.gabriel.demo_app.Data.ContentFetcher;
import com.adonis.gabriel.demo_app.Data.Model.ContentItem;
import com.adonis.gabriel.demo_app.Data.Model.ContentResponse;
import com.adonis.gabriel.demo_app.Fragments.Adapters.ContentAdapter;
import com.adonis.gabriel.demo_app.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllFragment extends ContentFragment {

    private static final String DEBUG_TAG = AllFragment.class.getSimpleName();

    public static final String FRAGMENT_TITLE = "ALL";

    public AllFragment() {

    }

    @Override
    protected int getContentPageNumber() {
        return 1;
    }
}
