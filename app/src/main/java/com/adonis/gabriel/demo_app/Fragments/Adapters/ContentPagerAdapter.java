package com.adonis.gabriel.demo_app.Fragments.Adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.adonis.gabriel.demo_app.Fragments.Discover.AllFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.ContentFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.CouponsFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.NewFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.SalesFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.UrgentFragment;
import com.google.android.gms.common.util.ArrayUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

//DAMN I'M PROWD OF THIS

/**
 * Adapter for the Content ViewPager
 * Each fragment that will displayed needs to define a public static String field FRAGMENT_TITLE
 * to have his title displayed in the tabs of the TabLayout
 *
 * If no such field is provided the name of the class will be displayed instead
 *
 * It uses reflection to get the field values
 */
public class ContentPagerAdapter extends FragmentStatePagerAdapter {

    private static final String DEBUG_TAG = ContentPagerAdapter.class.getSimpleName();
    private List<Class<? extends ContentFragment>> fragments;

    /**
     * Construct a new ContentPagerAdapter with the given FragmentManager (usually the one
     * that belongs to the parent Fragment or Activity) and a list of Fragments as its children
     * @param fm FragmentManager of the parent
     * @param fragments array of Class ojects of types that extend Fragment
     */
    public ContentPagerAdapter(FragmentManager fm, Class<? extends ContentFragment>[] fragments) {
        super(fm);
        this.fragments = fragments == null ? null : ArrayUtils.toArrayList(fragments);
    }


    @Override
    public Fragment getItem(int position) {
        if (position >= getCount()) {
            return null;
        }

        Class<? extends ContentFragment> fragmentType = fragments.get(position);
        ContentFragment fragment;
        try {
            // get the no args public constructor
            Constructor<? extends ContentFragment> fragmentConstructor = fragmentType.getConstructor();
            // create a new instance
            fragment = fragmentConstructor.newInstance();
        }
        catch (IllegalAccessException e) {
            fragment = null;
            Log.d(DEBUG_TAG, e.getMessage());
        } catch (NoSuchMethodException e) {
            fragment = null;
            Log.d(DEBUG_TAG, e.getMessage());
        } catch (InvocationTargetException e) {
            fragment = null;
            Log.d(DEBUG_TAG, e.getMessage());
        } catch (InstantiationException e) {
            fragment = null;
            Log.d(DEBUG_TAG, e.getMessage());
        }

        return fragment;
    }


    @Override
    public int getCount() {
        return this.fragments == null ? 0 : this.fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        if (fragments == null) {
            return "";
        }

        Class fragmentType = fragments.get(position);
        try {
            Field titleField = fragmentType.getField("FRAGMENT_TITLE");
            title = (String)fragmentType.getField("FRAGMENT_TITLE").get(null);
        }
        catch (NoSuchFieldException e) {
            title = fragmentType.getSimpleName();
            Log.d(DEBUG_TAG, e.getMessage());
        }
        catch (IllegalAccessException e) {
            title = fragmentType.getSimpleName();
            Log.d(DEBUG_TAG, e.getMessage());
        }

        return title;
    }

}
