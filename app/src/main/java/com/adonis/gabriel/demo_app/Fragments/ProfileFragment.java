package com.adonis.gabriel.demo_app.Fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adonis.gabriel.demo_app.Data.Model.UserEntity;
import com.adonis.gabriel.demo_app.Data.UserDatabase;
import com.adonis.gabriel.demo_app.Fragments.Adapters.ContentPagerAdapter;
import com.adonis.gabriel.demo_app.Fragments.Discover.AllFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.ContentFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.CouponsFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.NewFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.SalesFragment;
import com.adonis.gabriel.demo_app.Fragments.Discover.UrgentFragment;
import com.adonis.gabriel.demo_app.Login.AppLogin;
import com.adonis.gabriel.demo_app.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private static final int CONTENT_FRAGMENTS_COUNT = 5;
    public static final String FRAGMENT_TAG = "profile";

    @BindView(R.id.vp_content_glider)
    ViewPager mFragmentGlider;

    @BindView(R.id.profile_picture)
    CircleImageView mProfilePicture;

    @BindView(R.id.target_reward)
    CircleImageView mTargetReward;

    private UserDatabase mDatabase;

    private static final Class[] mViewPagerFragments = {AllFragment.class,
            CouponsFragment.class, NewFragment.class, SalesFragment.class, UrgentFragment.class};

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, inflatedView);

        mDatabase = UserDatabase.getInstance(getContext());

        FragmentManager fragmentManager = getChildFragmentManager();
        mFragmentGlider.setAdapter(new ContentPagerAdapter(fragmentManager, mViewPagerFragments));

        int loggedInUserId = AppLogin.getLoginUserId(getContext());
        new AsyncTask<Integer, Void, UserEntity>() {

            @Override
            protected UserEntity doInBackground(Integer... integers) {
                int userId = integers[0];
                UserEntity user = mDatabase.userDao().getUser(userId);
                return user;
            }

            @Override
            protected void onPostExecute(UserEntity userEntity) {
                super.onPostExecute(userEntity);
                String profilePictureUrl = userEntity.getProfilePicture();
                RequestCreator rc;
                if (profilePictureUrl == null || profilePictureUrl.isEmpty()) {
                    rc = Picasso.get().load(R.drawable.default_profile);
                }
                else {
                    rc = Picasso.get().load(profilePictureUrl);
                }
                rc.into(mProfilePicture);
            }
        }.execute(loggedInUserId);

        return inflatedView;
    }

}
