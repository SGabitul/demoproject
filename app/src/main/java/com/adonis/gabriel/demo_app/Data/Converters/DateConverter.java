package com.adonis.gabriel.demo_app.Data.Converters;


import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Converts Date representation to and from Long representation (Unix time) for storing in the database
 */
public class DateConverter {
    @TypeConverter
    public Long dateToLong(Date date) {
        if (date == null) {
            return null;
        }
        return date.getTime();
    }

    @TypeConverter
    public Date longToDate(Long dateTimestamp) {
        if (dateTimestamp == null) {
            return null;
        }
        return new Date(dateTimestamp);
    }

}
