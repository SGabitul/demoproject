package com.adonis.gabriel.demo_app.Data.Converters;

import android.arch.persistence.room.TypeConverter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Used to convert Bitmaps to and from byte arrays which are convertible to the BLOB data type
 */
public class BitmapConverter {

    @TypeConverter
    public static Bitmap bytesToBitmap(byte[] buffer) {
        if (buffer == null) {
            return null;
        }
        return BitmapFactory.decodeByteArray(buffer, 0, 0);
    }

    @TypeConverter
    public static byte[] bitmapToBytes(Bitmap image) {
        if (image == null) {
            return null;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }
}
