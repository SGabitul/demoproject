package com.adonis.gabriel.demo_app.Data.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.adonis.gabriel.demo_app.Data.Model.UserEntity;

import java.util.List;

@Dao
public interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(UserEntity user);

    @Query("SELECT * FROM users WHERE phone_number = :phoneNumber")
    UserEntity getUser(String phoneNumber);

    @Query("SELECT * FROM users WHERE id = :id")
    UserEntity getUser(int id);

    @Query("SELECT * FROM users WHERE phone_number = :phoneNumber AND password = :password")
    UserEntity validateUser(String phoneNumber, String password);

    @Delete()
    void deleteUser(UserEntity user);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateUser(UserEntity user);

}
