package com.adonis.gabriel.demo_app.Login;

import android.app.Activity;
import android.os.AsyncTask;

import com.adonis.gabriel.demo_app.Data.Model.UserEntity;
import com.adonis.gabriel.demo_app.Data.UserDatabase;

/**TODO add a error message module
 * it could have different error message types and show the appropriate message based on the type
*/
public final class Validation {

    public interface UserRegisteredCallback {
        void onUserRegistered(Activity currentActivity, UserEntity user);
        void onUserNotRegistered(Activity currentActivity, String phoneNumber);
    }

    /**
     * Verifies if the string represents a phone number i.e. if the string contains only 10 digits from 0 to 9
     * @param number string representation of the phone number
     * @return       true or false wether the given number is valid
     */
    public static boolean phoneNumberValid(String number) {
        if (number == null) {
            return false;
        }

        if (number.matches("^[0-9]{10}$")) {
            return true;
        }

        return false;
    }

    /**
     * Verifies if the password is valid i.e. it's length is at least 8 characters
     * @param password
     * @return true if the password is valid (longer than 8)
     */
    public static boolean passwordValid(String password) {
        if (password == null) {
            return false;
        }
        return password.length() >= 8;
    }

    /**
     * Checks if the given phoneNumber is given in the database
     * @param phoneNumber the target phone number
     * @param database the database that will be used for the look up
     * @return wether the phone number is registered
     */
    public static void isRegistered(final String phoneNumber, final UserDatabase database, final UserRegisteredCallback callback, final Activity currentActivity) {
        new AsyncTask<String, Void, UserEntity>() {
            @Override
            protected UserEntity doInBackground(String... args) {
                String phoneNumber = args[0];
                UserEntity user = database.userDao().getUser(phoneNumber);
                return user;
            }

            @Override
            protected void onPostExecute(UserEntity result) {
                if (result != null) {
                    callback.onUserRegistered(currentActivity, result);
                } else {
                    callback.onUserNotRegistered(currentActivity, phoneNumber);
                }
            }
        }.execute(phoneNumber);
    }

    /**
     * Checks if the password matches the phoneNumber in the database
     * @param phoneNumber
     * @param password
     * @param database
     * @return true if the user is registered in the database, false otherwise
     *
    public static void loginValid(String phoneNumber, String password, final UserDatabase database, final UserLoginValidCallback callback) {
        new AsyncTask<String, Void, UserEntity>() {
            @Override
            protected UserEntity doInBackground(String... args) {
                String phoneNumber = args[0];
                String password = args[1];
                return database.userDao().validateUser(phoneNumber, password);
            }

            @Override
            protected void onPostExecute(UserEntity result) {
                super.onPostExecute(result);
                if (result != null) {
                    callback.logindInvalid();
                }
                else {
                    callback.logindInvalid();
                }
            }
        }.execute(phoneNumber, password);
    }
    */


    /**
     * Checks if name is valid
     * @param name the last name
     * @return true if the name is not empty or not null, false otherwise
     */
    public static boolean nameValid(String name) {
        if (name == null || name.isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * Checks if the email has a valid format of text@text.com where text is a string of digits and
     * letters
     * @param email the email
     * @return true if the email has a valid format, false otherwise or if email is null
     */
    public static boolean emailValid(String email) {
        if (email == null || email.isEmpty()) {
            return false;
        }

        //IMP make this pattern less restrictive
        if (email.toLowerCase().matches("^[a-zA-Z0-9\\._-]+@[a-z0-9-]+(\\.[a-zA-Z]+)+$")) {
            return true;
        }

        return false;
    }
}
