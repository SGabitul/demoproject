package com.adonis.gabriel.demo_app.Data.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Represents a response item received from the data source of the Discover screen data, containing
 * a list of ContentItems which will be displayed
 */
public class ContentResponse {
    @SerializedName("page")
    @Expose
    public Integer page;
    @SerializedName("total_results")
    @Expose
    public Integer totalResults;
    @SerializedName("total_pages")
    @Expose
    public Integer totalPages;
    @SerializedName("results")
    @Expose
    public List<ContentItem> results;
}
