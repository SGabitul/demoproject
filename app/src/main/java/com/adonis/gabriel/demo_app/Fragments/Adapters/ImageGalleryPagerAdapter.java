package com.adonis.gabriel.demo_app.Fragments.Adapters;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.adonis.gabriel.demo_app.Fragments.Details.ImageHolderFragment;

public class ImageGalleryPagerAdapter extends FragmentStatePagerAdapter {


    private String imageUrl;

    public ImageGalleryPagerAdapter(FragmentManager fm, String image) {
        super(fm);
        this.imageUrl = image;
    }

    @Override
    public Fragment getItem(int i) {
        Bundle bundle = new Bundle();
        bundle.putString(ImageHolderFragment.IMAGE_KEY, imageUrl);
        ImageHolderFragment fragment = new ImageHolderFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
