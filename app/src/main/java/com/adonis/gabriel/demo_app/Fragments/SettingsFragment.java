package com.adonis.gabriel.demo_app.Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adonis.gabriel.demo_app.Data.Model.UserEntity;
import com.adonis.gabriel.demo_app.Data.UserDatabase;
import com.adonis.gabriel.demo_app.Login.AppLogin;
import com.adonis.gabriel.demo_app.Login.Validation;
import com.adonis.gabriel.demo_app.R;
import com.adonis.gabriel.demo_app.SignInUpActivity;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsFragment extends Fragment {

    private static final String DEBUG_TAG = SettingsFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = "settings";

    public static class DatePickerDialogFragment extends DialogFragment {

        public static final String DATE_PICKER_TAG = "date_picker";
        public static final String DATE_SET_LISTENER_KEY = "date_set_listener";

        private DatePickerDialog.OnDateSetListener dateSetListener;

        public void setDateSetListener(DatePickerDialog.OnDateSetListener dateSetListener) {
            this.dateSetListener = dateSetListener;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            Calendar calendar = Calendar.getInstance();
            return new DatePickerDialog(getActivity(), dateSetListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
        }
    }

    public static class ChangePasswordDialogFragment extends DialogFragment {

        public interface OnConfirmBinder {
            void bind(String password);
        }

        public static final String CHANGE_PASSWORD_TAG = "change_password";

        private View rootView;

        private OnConfirmBinder onConfirmBinder;

        private AlertDialog changePasswordAlertDialog;

        private DialogInterface.OnShowListener onShowListener = new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button button = changePasswordAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(onConfirmClickListener);
            }
        };

        private View.OnClickListener onConfirmClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText passwordField = (EditText)rootView.findViewById(R.id.password );
                EditText passwordConfirmField = (EditText)rootView.findViewById(R.id.password_confirm);

                String password = passwordField.getText().toString();
                String passwordConfirm = passwordConfirmField.getText().toString();

                if (Validation.passwordValid(password) && password.equals(passwordConfirm)) {
                    onConfirmBinder.bind(password);
                    changePasswordAlertDialog.dismiss();
                }
                else {
                    Toast.makeText(getContext(), "Password is not valid", Toast.LENGTH_SHORT).show();
                }
            }
        };

        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            //MARE OM
            rootView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_change_password, null);

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Enter your new password")
                    .setView(rootView)
                    .setPositiveButton(R.string.action_hint_confirm, null)
                    .setNegativeButton(R.string.action_hint_cancel,null);

            this.changePasswordAlertDialog = builder.create();
            this.changePasswordAlertDialog.setOnShowListener(this.onShowListener);

            return changePasswordAlertDialog;

        }

        private void setOnConfirmBinder(OnConfirmBinder onConfirmBinder) {
            this.onConfirmBinder = onConfirmBinder;
        }

        public OnConfirmBinder getOnConfirmBinder() {
            return onConfirmBinder;
        }


    }

    @BindView(R.id.profile_picture)
    ImageView mProfilePicture;

    @BindView(R.id.tv_phone_number)
    TextView mPhoneNumber;

    @BindView(R.id.tv_first_name)
    EditText mFirstName;

    @BindView(R.id.tv_last_name)
    EditText mLastName;

    @BindView(R.id.tv_birth_date)
    TextView mBirthDate;
    private static final SimpleDateFormat mDateFormat = new SimpleDateFormat("dd MMMM yyyy");

    @BindView(R.id.tv_email)
    EditText mEmail;

    @BindView(R.id.tv_password)
    TextView mPassword;

    @BindView(R.id.bt_male)
    Button mGenderButtonMale;

    @BindView(R.id.bt_female)
    Button mGenderButtonFemale;

    private String mSelectedGender;

    private UserDatabase mDatabase;
    private UserEntity mData;

    private DatePickerDialogFragment mDatePickerDialog;
    private ChangePasswordDialogFragment mChangePasswordDialogFragment;

    public SettingsFragment() {
        // Required empty public constructor
    }

    private String getSelectedGender() {
        return mSelectedGender;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflatedView = inflater.inflate(R.layout.fragment_settings, container, false);

        ButterKnife.bind(this, inflatedView);

        mDatabase = UserDatabase.getInstance(getContext());
        final int loggedInUserId = AppLogin.getLoginUserId(getContext());
        if (loggedInUserId != AppLogin.LOGIN_USER_MISSING) {
            new AsyncTask<Integer, Void, Void>() {

                @Override
                protected Void doInBackground(Integer... loggedInUser) {
                    mData = mDatabase.userDao().getUser(loggedInUserId);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    displayData();
                }
            }.execute(loggedInUserId);
        }
        else {
            mData = null;
        }

        mDatePickerDialog = new DatePickerDialogFragment();
        mDatePickerDialog.setDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                Date date = calendar.getTime();
                mBirthDate.setText(formatDate(date));
            }
        });

        mChangePasswordDialogFragment = new ChangePasswordDialogFragment();
        mChangePasswordDialogFragment.setOnConfirmBinder(new ChangePasswordDialogFragment.OnConfirmBinder() {
            @Override
            public void bind(String password) {
                mPassword.setText(password);
            }
        });


        return inflatedView;
    }

    private String formatDate(Date date) {
        if (date == null) {
            return "";
        }

        return mDateFormat.format(date);
    }

    private Date formatDate(String date) {
        if (date == null || date.isEmpty()) {
            return null;
        }

        try {
            return mDateFormat.parse(date);
        }
        catch (ParseException e) {
            Log.d(DEBUG_TAG,"This shouldn't happen but ok");
            return null;
        }
    }

    private void displayData() {
        if (mData != null) {
            mPhoneNumber.setText(mData.getPhoneNumber());
            mFirstName.setText(mData.getFirstName());
            mLastName.setText(mData.getLastName());
            mBirthDate.setText(formatDate(mData.getBirthDate()));
            mEmail.setText(mData.getEmail());
            mPassword.setText(mData.getPassword());

            String gender = mData.getGender();
            if (gender != null ) {
                if (gender.equals(UserEntity.GENDER_MALE)) {
                    selectButton(mGenderButtonMale);
                } else {
                    selectButton(mGenderButtonFemale);
                }
            }

            bindProfilePicture(mData.getProfilePicture());
        }
    }

    private void bindProfilePicture(String url) {
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.default_profile)
                .error(R.drawable.default_profile)
                .into(mProfilePicture);
    }


    @OnClick(R.id.bt_save)
    void updateUser() {
        updateData();
        if (dataValid()) {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected Void doInBackground(Void... voids) {
                    mDatabase.userDao().updateUser(mData);
                    return null;
                }
            }.execute();
            Toast.makeText(getContext(), "Data updated", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateData() {
        mData.setFirstName(mFirstName.getText().toString());
        mData.setLastName(mLastName.getText().toString());
        mData.setBirthDate(formatDate(mBirthDate.getText().toString()));
        mData.setEmail(mEmail.getText().toString());
        mData.setPassword(mPassword.getText().toString());
        mData.setGender(getSelectedGender());
    }

    private boolean dataValid() {
        Context context = getContext();
        if (!Validation.nameValid(mData.getFirstName())) {
            Toast.makeText(context, "First name should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Validation.nameValid(mData.getLastName())) {
            Toast.makeText(context, "Last name should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mData.getBirthDate() == null) {
            Toast.makeText(context, "Birthday should not be empty", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Validation.emailValid(mData.getEmail())) {
            Toast.makeText(context, "Email not valid", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (getSelectedGender() == null) {
            Toast.makeText(context, "No gender selected", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @OnClick(R.id.tv_birth_date)
    void openDatePickerDialog() {
        mDatePickerDialog.show(getChildFragmentManager(), DatePickerDialogFragment.DATE_PICKER_TAG);
    }

    @OnClick({R.id.bt_male, R.id.bt_female})
    void selectGender(Button selectedGenderButton) {
        if (selectedGenderButton == mGenderButtonFemale) {
            mSelectedGender = UserEntity.GENDER_FEMALE;
        }
        else {
            mSelectedGender = UserEntity.GENDER_MALE;
        }

        selectButton(selectedGenderButton);
    }

    private void selectButton(Button button) {
        button.setTextColor(Color.BLACK);

        if (button == mGenderButtonFemale) {
            button.setBackgroundResource(R.drawable.card_background_right);
            deselectButton(mGenderButtonMale);
            mSelectedGender = UserEntity.GENDER_FEMALE;
        } else {
            button.setBackgroundResource(R.drawable.card_background_left);
            deselectButton(mGenderButtonFemale);
            mSelectedGender = UserEntity.GENDER_MALE;
        }
    }

    private void deselectButton(Button button) {
        button.setBackground(new ColorDrawable(Color.TRANSPARENT));
        button.setTextColor(Color.WHITE);
    }

    @OnClick(R.id.change_password)
    void changePassword() {
        mChangePasswordDialogFragment.show(getChildFragmentManager(), ChangePasswordDialogFragment.CHANGE_PASSWORD_TAG);
    }

    @OnClick(R.id.bt_log_out)
    void logOut() {
        AppLogin.setLoginState(getContext(), AppLogin.LOGIN_STATE_GUEST, null);
        Intent startLoginScreenIntent = new Intent(getContext(), SignInUpActivity.class);
        startActivity(startLoginScreenIntent);
        Activity parentActivity = getActivity();
        if (parentActivity != null) {
            parentActivity.finish();
        }
        else {
            Log.d(DEBUG_TAG, "This should have never happened");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mDatabase.close();
    }
}
