package com.adonis.gabriel.demo_app.Data;

import android.os.AsyncTask;
import android.util.Log;

import com.adonis.gabriel.demo_app.Data.Model.ContentItem;
import com.adonis.gabriel.demo_app.Data.Model.ContentResponse;
import com.adonis.gabriel.demo_app.Fragments.Discover.AllFragment;
import com.adonis.gabriel.demo_app.MainActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ContentFetcher {

    public static final String DEBUG_TAG = ContentService.class.getSimpleName();

    public static final String CONTENT_BASE_URL =  "https://api.themoviedb.org/3/";
    public static final String POSTER_BASE_URL = "https://image.tmdb.org/t/p/w500/";
    public static final String CONTENT_KEY = "1906833e4f7429ebcd27cb7a04da7c0a";
    public static final String PAGE_NUMBER_KEY = "page_number";

    public static final int PAGE_NUMBER_DEFAULT = 1;

    public interface ContentService {
        @GET("discover/movie")
        Call<ContentResponse> getContent(@Query("api_key") String key, @Query("page") int page);
    }

    private static Call<ContentResponse> getContentCall(int page) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CONTENT_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ContentService service = retrofit.create(ContentService.class);
        return service.getContent(CONTENT_KEY, page);
    }

    public static Call<ContentResponse> enqueueFetch(int page, Callback<ContentResponse> callback) {
        Call<ContentResponse> contentResponseCall = getContentCall(page);
        contentResponseCall.enqueue(callback);
        return contentResponseCall;
    }
}
