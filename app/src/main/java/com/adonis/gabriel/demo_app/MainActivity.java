package com.adonis.gabriel.demo_app;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.adonis.gabriel.demo_app.CustomViews.AHBottomNavigationHelper;
import com.adonis.gabriel.demo_app.CustomViews.BottomNavigation;
import com.adonis.gabriel.demo_app.CustomViews.CaptureActivityPortrait;
import com.adonis.gabriel.demo_app.CustomViews.CircleTransform;
import com.adonis.gabriel.demo_app.CustomViews.EventDispatcher;
import com.adonis.gabriel.demo_app.Data.Model.UserEntity;
import com.adonis.gabriel.demo_app.Data.UserDatabase;
import com.adonis.gabriel.demo_app.Fragments.DetailsFragment;
import com.adonis.gabriel.demo_app.Fragments.SettingsFragment;
import com.adonis.gabriel.demo_app.Fragments.DiscoverFragment;
import com.adonis.gabriel.demo_app.Fragments.ProfileFragment;
import com.adonis.gabriel.demo_app.Fragments.StoresFragment;
import com.adonis.gabriel.demo_app.Login.AppLogin;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener, FragmentManager.OnBackStackChangedListener,
        EventDispatcher {

    private static final String DEBUG_TAG = MainActivity.class.getSimpleName();


    @BindView(R.id.nv_bottom_tabs)
    AHBottomNavigation mBottomTabs;

    /**
     * Children of this activity can subscribe to this LiveData object which will be updated with the
     * corresponding event identifier
     */
    MutableLiveData<Integer> mEventDispatcher;

    AHBottomNavigation.OnTabSelectedListener mBottomTabsSelectListener = new AHBottomNavigation.OnTabSelectedListener() {
        @Override
        public boolean onTabSelected(int position, boolean wasSelected) {
            final int POSITION_DISCOVER = 0;
            final int POSITION_SCAN_QR = 1;
            final int POSITION_STORES = 2;
            final int POSITION_PROFILE = 3;

            switch(position) {
                case POSITION_DISCOVER :
                    startScreen(R.layout.fragment_discover);
                    break;

                case POSITION_SCAN_QR:
                    startScreen(R.layout.fragment_coupons);
                    break;

                case POSITION_STORES :
                    startScreen(R.layout.fragment_stores);
                    break;

                case POSITION_PROFILE :
                    startScreen(R.layout.fragment_profile);
                    break;

                default :
                    Toast.makeText(getBaseContext(), "Why is this happening? HOW?!", Toast.LENGTH_SHORT).show();
            }

            lastSelectedItemId = mBottomNavigationHelper.getSelectedItemId();
            return true;
        }
    };

    BottomNavigation mBottomNavigationHelper;

    FragmentManager mFragmentManager;

    Menu mOptionsMenu;

    ActionBar mActionBar;

    UserDatabase mDatabase;

    int lastSelectedItemId = R.id.item_discover;

    /**
     * Used to tell if the OnBackStackChange event was a add or a pop
     */
    private int mBackstackSize;

    //used for the QR scanner
    IntentIntegrator integrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mBackstackSize = 0;

        mDatabase = UserDatabase.getInstance(this);

        mEventDispatcher = new MutableLiveData<>();

        mActionBar = getSupportActionBar();
        initializeActionBar();

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(this);

        int loggedInUserId = AppLogin.getLoginUserId(this);

        mBottomNavigationHelper = new AHBottomNavigationHelper(mBottomTabs, this, R.menu.bottom_bar);
        initializeBottomNavigation();

        final Context context = this;
        new AsyncTask<Integer, Void, UserEntity>() {

            @Override
            protected UserEntity doInBackground(Integer... ids) {
                int userId = ids[0];
                UserEntity user = null;
                if (userId != AppLogin.LOGIN_USER_MISSING) {
                    user = mDatabase.userDao().getUser(userId);
                }
                return user;
            }

            @Override
            protected void onPostExecute(UserEntity user) {
                super.onPostExecute(user);
                RequestCreator rc;

                if (user == null) {
                    rc = Picasso.get().load(R.drawable.default_profile);
                }
                else {
                    String profilePictureUrl = user.getProfilePicture();
                    if (profilePictureUrl == null || profilePictureUrl.isEmpty()) {
                        rc = Picasso.get().load(R.drawable.default_profile);
                    }
                    else {
                        rc = Picasso.get().load(profilePictureUrl);
                    }
                }
                rc.transform(new CircleTransform())
                  .into(new Target() {

                      @Override
                      public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                          BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
                          mBottomNavigationHelper.setItemIcon(drawable, R.id.item_profile);
                      }

                      @Override
                      public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                      }

                      @Override
                      public void onPrepareLoad(Drawable placeHolderDrawable) {

                      }

                  });
            }
        }.execute(loggedInUserId);

        integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
    }

    private void initializeBottomNavigation() {
        mBottomNavigationHelper.setOnNavigationItemSelected(mBottomTabsSelectListener);
        mBottomNavigationHelper.setBackgroundResource(R.color.menuBackground);
        ColorStateList bottomColors = ContextCompat.getColorStateList(this, R.color.bottom_bar_items);
        mBottomNavigationHelper.setTintList(bottomColors);
        mBottomNavigationHelper.disableTint(this, R.id.item_profile);
        mBottomNavigationHelper.hideTitles();
        // set startup fragment
    }

    private void initializeActionBar() {
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowCustomEnabled(true);
        int color = ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getTheme());
        mActionBar.setBackgroundDrawable(new ColorDrawable(color));
        mActionBar.setHomeAsUpIndicator(R.drawable.arrow);
        mActionBar.setCustomView(R.layout.action_bar_discover);
    }

    private void setActionBar(int actionBarLayoutId) {
        mActionBar.setCustomView(actionBarLayoutId);

        // hide settings icon and show search icon
        switch (actionBarLayoutId) {
            case R.layout.action_bar_discover:
                hideMenuItem(R.id.item_settings);
                showMenuItem(R.id.item_search);
                break;

            case R.layout.action_bar_stores:
                hideMenuItem(R.id.item_settings);
                hideMenuItem(R.id.item_search);
                break;

            case R.layout.action_bar_profile:
                hideMenuItem(R.id.item_search);
                showMenuItem(R.id.item_settings);
                break;

            case R.layout.action_bar_settings :
                hideMenuItem(R.id.item_search);
                hideMenuItem(R.id.item_settings);
                linkBackButton(actionBarLayoutId);
                break;

            case R.layout.action_bar_details :
                hideMenuItem(R.id.item_search);
                hideMenuItem(R.id.item_settings);
                linkBackButton(actionBarLayoutId);
                break;
        }

    }

    private void linkBackButton(int layoutId) {
        View backButton = mActionBar.getCustomView().findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFragmentManager.popBackStack();
            }
        });
    }

    private void hideMenuItem(int menuItemId) {
        if (mOptionsMenu == null)
            return;

        MenuItem menuItem = mOptionsMenu.findItem(menuItemId);
        if (menuItem != null) {
            menuItem.setVisible(false);
        }
    }

    private void showMenuItem(int menuItemId) {
        if (mOptionsMenu == null)
            return;

        MenuItem menuItem = mOptionsMenu.findItem(menuItemId);
        if (menuItem != null) {
            menuItem.setVisible(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mOptionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        return true;
    }

    //TODO implement button presses
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        //TODO move all the fragment creations and stuffs in a class
        if (itemId == R.id.item_settings) {
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            transaction.replace(R.id.fragment_container, new SettingsFragment());
            transaction.addToBackStack(SettingsFragment.FRAGMENT_TAG);
            transaction.commit();
        } else if (itemId == R.id.item_search) {
            mEventDispatcher.setValue(EVENT_ID_SEARCH_CLICKED);
        }

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        int selectedItemId = mBottomNavigationHelper.getSelectedItemId();
        if (selectedItemId == itemId) {
            return false;
        }

        switch(itemId) {
            case R.id.item_discover :
                startScreen(R.layout.fragment_discover);
                break;

            case R.id.item_scan_qr:
                startScreen(R.layout.fragment_coupons);
                break;

            case R.id.item_stores :
                startScreen(R.layout.fragment_stores);
                break;

            case R.id.item_profile :
                startScreen(R.layout.fragment_profile);
                break;

            default :
                Toast.makeText(this, "Why is this happening? HOW?!", Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    /**
     * Creates a new fragment and updates the action bar to fit the new screen
     * @param fragmentLayoutId resource layout id of the fragment you want to start
     * @return whether the fragment was started (e.g. when
     */
    private boolean startScreen(int fragmentLayoutId) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        String fragmentTag;

        Fragment newFragment;
        switch (fragmentLayoutId) {
            case R.layout.fragment_discover :
                newFragment = new DiscoverFragment();
                fragmentTag = DiscoverFragment.FRAGMENT_TAG;
                break;

            case R.layout.fragment_coupons :
                scanQrCode();
                return false;

            case R.layout.fragment_stores :
                newFragment = new StoresFragment();
                ((StoresFragment)newFragment).getMapAsync((StoresFragment)newFragment);
                fragmentTag = StoresFragment.FRAGMENT_TAG;
                break;

            case R.layout.fragment_profile :
                if (AppLogin.getLoginState(this).equals(AppLogin.LOGIN_STATE_GUEST)) {
                    Toast.makeText(this, "You are not logged in", Toast.LENGTH_SHORT).show();
                    return false;
                }
                newFragment = new ProfileFragment();
                fragmentTag = ProfileFragment.FRAGMENT_TAG;
                break;

            default :
                newFragment = null;
                return false;
        }

        FragmentManager.BackStackEntry backStackEntry = getTopBackstackEntry();
        // don't allow consecutive duplicates on the backstack
        if (backStackEntry != null && backStackEntry.getName().equals(fragmentTag)) {
            return false;
        }

        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(fragmentTag);
        transaction.commit();
        return true;
    }

    private FragmentManager.BackStackEntry getTopBackstackEntry() {
        int backstackSize = mFragmentManager.getBackStackEntryCount();
        if (backstackSize == 0) {
             return null;
        }
        return mFragmentManager.getBackStackEntryAt(backstackSize - 1);
    }



    /**
     * Opens a new activity that will scan for a QR code
     */
    private void scanQrCode() {
        integrator.initiateScan();
    }

    private IntentResult catchQrScanResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }
            else {
                String url = result.getContents();
                // used for validation
                Uri targetUri = Uri.parse(url);
                if (targetUri.getScheme() == null) {
                    Toast.makeText(this, "Invalid QR", Toast.LENGTH_LONG).show();
                }
                else {
                    openLinkInBrowser(url);
                }
            }
        }

        return result;
    }

    private void openLinkInBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // if the scan was not successful ignore it and let the base functionality through
        if (catchQrScanResult(requestCode, resultCode, data) == null) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * We mostly want to change the action bar to match the current fragment
     */
    @Override
    public void onBackStackChanged() {
        int backStackSize = mFragmentManager.getBackStackEntryCount();
        if (backStackSize == 0) {
            finish();
            return;
        }

        FragmentManager.BackStackEntry backStackEntry = mFragmentManager.getBackStackEntryAt(backStackSize - 1);
        String entryName = backStackEntry.getName();
        // if last operation was a pop
        //TODO this... this is so inefficient
        if (backStackSize < mBackstackSize) {
            // trying to return to the details screen from another fragment will pop it and show the discover screen instead
            if (entryName.equals(DetailsFragment.FRAGMENT_TAG)) {
                mFragmentManager.popBackStack();
            }
        }

        //Prints backstack to log
        StringBuilder backstackViewBuilder = new StringBuilder("\n");
        for (int i = 0; i < backStackSize; i++) {
            backstackViewBuilder.append(mFragmentManager.getBackStackEntryAt(i).getName());
            backstackViewBuilder.append("\n");
        }
        Log.d(DEBUG_TAG, backstackViewBuilder.toString());

        switch (entryName) {
            case DiscoverFragment.FRAGMENT_TAG :
                setActionBar(R.layout.action_bar_discover);
                mBottomNavigationHelper.setSelectedItemId(R.id.item_discover);
                break;

            case ProfileFragment.FRAGMENT_TAG :
                setActionBar(R.layout.action_bar_profile);
                mBottomNavigationHelper.setSelectedItemId(R.id.item_profile);
                break;

            case SettingsFragment.FRAGMENT_TAG :
                setActionBar(R.layout.action_bar_settings);
                break;

            case StoresFragment.FRAGMENT_TAG :
                setActionBar(R.layout.action_bar_stores);
                mBottomNavigationHelper.setSelectedItemId(R.id.item_stores);
                break;

            case DetailsFragment.FRAGMENT_TAG :
                setActionBar(R.layout.action_bar_details);
                break;
        }

        mBackstackSize = backStackSize;
    }

    @Override
    public MutableLiveData<Integer> getEventDispatcher() {
        return mEventDispatcher;
    }

    /**
     * Convenience method used to start this activity (because this Activity can have different
     * parent activities depending on user input)
     * @param parent the parent context that will start this activity
     */
    public static void startMainActivity(Activity parent) {
        Intent intent = new Intent(parent, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        parent.startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBottomNavigationHelper.setSelectedItemId(lastSelectedItemId);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mDatabase.close();
    }
}
