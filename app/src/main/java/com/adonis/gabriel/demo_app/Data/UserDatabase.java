package com.adonis.gabriel.demo_app.Data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.adonis.gabriel.demo_app.Data.Converters.BitmapConverter;
import com.adonis.gabriel.demo_app.Data.Converters.DateConverter;
import com.adonis.gabriel.demo_app.Data.Dao.UserDao;
import com.adonis.gabriel.demo_app.Data.Model.UserEntity;

@Database(entities = {UserEntity.class}, exportSchema = false, version = UserDatabase.DATABASE_VERSION)
@TypeConverters({DateConverter.class})
public abstract class UserDatabase extends RoomDatabase {

    public static final int DATABASE_VERSION = 1;

    private static final Object LOCK = new Object();

    public static final String DATABASE_NAME = "demo_database";

    private static UserDatabase sInstance = null;

    public static UserDatabase getInstance(Context context) {
        if (sInstance == null) {
            /* Make sure that no multiple instance creations are happening at the same time
             */
            synchronized (LOCK) {
                Log.d(UserDatabase.class.getSimpleName(), "Database will be built now");
                sInstance = Room.databaseBuilder(context.getApplicationContext(), UserDatabase.class, DATABASE_NAME)
                        .build();
            }
        }

        return sInstance;
    }

    @Override
    public void close() {
        super.close();
        sInstance = null;
    }

    public abstract UserDao userDao();
}
