package com.adonis.gabriel.demo_app.Fragments.Discover;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adonis.gabriel.demo_app.R;

public class SalesFragment extends ContentFragment {

    public static final String FRAGMENT_TITLE = "SALES";

    public SalesFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getContentPageNumber() {
        return 4;
    }
}
