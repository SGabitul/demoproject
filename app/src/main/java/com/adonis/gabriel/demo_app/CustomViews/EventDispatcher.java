package com.adonis.gabriel.demo_app.CustomViews;

import android.arch.lifecycle.MutableLiveData;

public interface EventDispatcher {

    public static final Integer EVENT_ID_SEARCH_CLICKED = 1;
    public static final Integer EVENT_ID_NONE = 0;

    MutableLiveData<Integer> getEventDispatcher();
}
