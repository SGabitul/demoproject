package com.adonis.gabriel.demo_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.adonis.gabriel.demo_app.Login.AppLogin;
import com.adonis.gabriel.demo_app.Login.Validation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChoosePasswordActivity extends AppCompatActivity {

    @BindView(R.id.tv_password)
    EditText mPassword;

    @BindView(R.id.tv_password_confirm)
    EditText mPasswordConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_password);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.bt_next)
    public void next() {
        String password = mPassword.getText().toString();
        String passwordConfirm = mPasswordConfirm.getText().toString();

        if (password.equals(passwordConfirm)) {
            if (Validation.passwordValid(password)) {
                Intent intent = getIntent();
                // put the password in the same intent that started this activity and pass it forward
                intent.putExtra(getString(R.string.tag_password), password);
                intent.setClass(this, UserDataActivity.class);
                startActivity(intent);
            }
            else {
                Toast.makeText(this, R.string.password_invalid, Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(this, R.string.password_nomatch, Toast.LENGTH_SHORT).show();
        }
    }

}
