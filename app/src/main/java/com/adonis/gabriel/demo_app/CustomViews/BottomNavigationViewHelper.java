package com.adonis.gabriel.demo_app.CustomViews;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;

/*
//TODO write documentation for this and refactor things around
public class BottomNavigationViewHelper implements BottomNavigation {

    @Override
    public void setOnNavigationItemSelected(Object listener) {
        BottomNavigationView.OnNavigationItemSelectedListener actualListener = null;
        if (listener instanceof BottomNavigationView.OnNavigationItemSelectedListener) {
            actualListener = (BottomNavigationView.OnNavigationItemSelectedListener)listener;
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(actualListener);
    }

    @Override
    public void setTintList(ColorStateList list) {
        bottomNavigationView.setItemIconTintList(list);
    }

    @Override
    public void setItemTintList(ColorStateList list, int itemId) {
        return;
    }

    @Override
    public void setSelectedItemId(int id) {
        bottomNavigationView.setSelectedItemId(id);
    }

    @Override
    public int getSelectedItemId() {
        return bottomNavigationView.getSelectedItemId();
    }

    @Override
    public void setItemIcon(@NonNull Drawable icon, @NonNull int menuItemId) {
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.findItem(menuItemId);
        menuItem.setIcon(icon);
    }

    @Override
    public Drawable getItemIcon(@NonNull int menuItemId) {
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.findItem(menuItemId);
        return menuItem.getIcon();
    }


    @Override
    public Menu getMenu() {
        return bottomNavigationView.getMenu();
    }

    @Override
    public void inflateMenu(Context context, @NonNull int resId) {
        bottomNavigationView.inflateMenu(resId);
    }

    @Override
    public void setBackground(Drawable background) {

    }

    @Override
    public void setBackgroundColor(int color) {

    }

    @Override
    public void setBackgroundResource(int resId) {

    }

    private BottomNavigationView bottomNavigationView;

    public BottomNavigationViewHelper(@NonNull BottomNavigationView view) {
        this.bottomNavigationView = view;
    }

    public BottomNavigationView getBottomNavigationView() {
        return bottomNavigationView;
    }

    public void setBottomNavigationView(BottomNavigationView bottomNavigationView) {
        this.bottomNavigationView = bottomNavigationView;
    }



}

*/